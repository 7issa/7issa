# -*- coding: utf-8 -*-
#!/usr/bin/python3

#import _mysql
import sys , subprocess
#from random import randint
import datetime
#from lxml import html
import requests
#from bs4 import BeautifulSoup
import re ,os
import MySQLdb as mdb
import mysql.connector

#ip_host="199.245.58.235"
#ip_host="185.207.112.71"
ip_host="199.245.58.158"
mysql_user="cicada3301"
mysql_pass="binpass"
mysql_db="all_bin_small"

#mysql_db="all_bin"


def fix_tor():
	print(" FIXING TOR  CONNECTION : ",end='',flush=True)
	with open(os.devnull, 'wb') as hide_output:
		exit_code = subprocess.Popen(['service', "tor", 'restart'], stdout=hide_output, stderr=hide_output).wait()
		if exit_code==0:
			print("TOR FIXED")
			#check_connect_mysql()



def check_connect_mysql():
	print(" CHECK SQL  CONNECTION       : ",end='',flush=True)
	try:
		mydb = mysql.connector.connect(host=ip_host,user=mysql_user,passwd=mysql_pass,database=mysql_db)
		mycursor = mydb.cursor()
		print("MYSQL CONNECTED OK ")
		return mycursor
	except  Exception as e :
		print("FIELED")
		print(" SQL ERROR CONNECTION        : ",end='',flush=True)
		fix_mysql(str(e))



def fix_mysql(e):
	if "Connection refused" in e:
		print("Connection refused")
		print(" FIXING SQL ERROR CONNECTION : ",end='',flush=True)
		with open(os.devnull, 'wb') as hide_output:
			exit_code = subprocess.Popen(['service', "mysql", 'restart'], stdout=hide_output, stderr=hide_output).wait()
			if exit_code==0:
				print("FIXED")
				check_connect_mysql()



def search_contry_bin(country):
	mydb = mysql.connector.connect(host=ip_host,user=mysql_user,passwd=mysql_pass,database=mysql_db)
	mycursor = mydb.cursor()
	qury="SELECT * from ta_bin WHERE Country_Code = %s"
	bin_rnd=country
	#print(bin_rnd)
	mycursor.execute(qury,(bin_rnd,))
	myresult = mycursor.fetchall()
	#print(len(myresult))
	return myresult



def search_i_d_bin(index):
	
	mydb = mysql.connector.connect(host=ip_host,user=mysql_user,passwd=mysql_pass,database=mysql_db)
	mycursor = mydb.cursor()
	qury="SELECT * from ta_bin WHERE ID = %s"
	bin_rnd=index
	#print(bin_rnd)
	mycursor.execute(qury,(bin_rnd,))
	myresult = mycursor.fetchall()
	#print(len(myresult))
	return myresult
