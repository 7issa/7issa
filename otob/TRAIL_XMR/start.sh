#!/bin/bash
export LC_ALL='en_US.utf8'
while :
do
	echo "Press [CTRL+C] to stop.."
	sleep 1
	rm -rf /tmp/*
	python3 oceon.py
done
